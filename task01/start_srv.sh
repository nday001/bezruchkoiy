#!/bin/sh

# собираем образ
docker build -t bezruchkoiy/srv:v1 ./test

# запускаю образ
docker run -d --name=bezruchkoiy_srv -p 12345:12345 bezruchkoiy/srv:v1

# проверяю готовность контейнера
for i in {1..5}; do
    docker exec bezruchkoiy_srv echo 'ok' > /dev/null
    if [[ $? -eq '0' ]]; then
        echo 'docker start'
        break
    fi
    if [[ $i -eq '5' ]]; then
        echo 'docker don`t start'
    else
        sleep 2
    fi
done
