#!/bin/bash

# останавливаю контейнер
docker stop bezruchkoiy_srv

# удаляю контейнер
docker rm bezruchkoiy_srv

# удаляю образ
docker rmi bezruchkoiy/srv:v1
