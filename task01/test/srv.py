#!/usr/bin/env python3

# подключаем модуль socket
import socket

# создаем сокет
srv_sock = socket.socket()
print ('create socket')

# хост не указываю чтобы сервер был доступен для всех интерфейсов, порт указываю из тз
srv_host = ''
srv_port = 12345
srv_sock.bind((srv_host, srv_port))

# включаем сокет в режим прослушивания с максимальным количеством клиентов - 1
srv_sock.listen(1)

while True:
    # чтобы принять подключение используем метод accept, который возвращает кортеж с двумя элементами: новый сокет и адрес клиента.
    # кортежи (тип tuple) — это неизменяемый тип данных в Python,
    # который используется для хранения упорядоченной последовательности элементов
    cli_sock, cli_addr = srv_sock.accept()
    print ('connect ', cli_addr)

    while True:
        # получаем данные методом recv, который в качестве аргумента принимает количество байт для чтения
        data = cli_sock.recv(1024)
        if not data:
            print ('no data')
            break
        print ('received "', data, '"')
        cli_sock.send("ok".encode('utf-8'))
        print ('send "', data, '"')

    # закрываем сокет
    cli_sock.close()
